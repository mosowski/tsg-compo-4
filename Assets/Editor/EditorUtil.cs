﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class EditorUtil : MonoBehaviour {

	public static IEnumerable<string> GetFilesInDirectory(string directoryPath, bool recursive = true) {
		string systemProjectPath = Application.dataPath;
		string systemDirectoryPath = systemProjectPath.Substring(0, systemProjectPath.Length-6) + directoryPath;
		foreach (string filePath in GetFilesInSystemDirectory(systemDirectoryPath, recursive)) {
			yield return filePath;
		}
	}

	public static IEnumerable<string> GetFilesInSystemDirectory(string directoryPath, bool recursive = true) {
		string[] filesPaths = Directory.GetFiles(directoryPath);
		foreach (string filePath in filesPaths) {
			yield return filePath;
		}
		if (recursive) {
			string[] subdirs = Directory.GetDirectories(directoryPath);
			foreach (string subdirectory in subdirs) {
				foreach (string filePath in GetFilesInSystemDirectory(subdirectory)) { 
					yield return filePath; 
				}
			}
		}
	}

	public static IEnumerable<Object> GetAssetsInDirectory(string directoryPath, bool recursive = true) {
		string systemProjectPath = Application.dataPath;
		foreach (string systemFilePath in GetFilesInDirectory(directoryPath, recursive)) {
			string localAssetPath = systemFilePath.Substring(systemProjectPath.Length-6);

			Object asset = AssetDatabase.LoadAssetAtPath(localAssetPath, typeof(Object));
			yield return asset;
		}
	}
}
