﻿using UnityEngine;
using System.Collections;
using System;

public class HUD : MonoBehaviour {

	UILabel time;
	UILabel player1;
	UILabel player2;
	UILabel status;

	// Use this for initialization
	void Start () {
		time = gameObject.FindRecursive("Time").GetComponent<UILabel>();
		player1 = gameObject.FindRecursive("Player1").GetComponent<UILabel>();
		player2 = gameObject.FindRecursive("Player2").GetComponent<UILabel>();
		status = gameObject.FindRecursive("Status").GetComponent<UILabel>();
	}
	
	// Update is called once per frame
	void Update () {
		float t = (Tron._.maxTicks - Tron._.currentTickNum) * Tron._.TickTime;
		int seconds = Mathf.RoundToInt(t);
		time.text = String.Format("{0:0}:{1:00}", seconds / 60, seconds % 60);

		player1.text = Tron._.pointsByPlayerId[Tron._.players[0]].ToString();
		player2.text = Tron._.pointsByPlayerId[Tron._.players[1]].ToString();

		if (Tron._.EndConditionMet()) {
			status.gameObject.active = true;
			int score = Tron._.pointsByPlayerId["Player1"];
			bool won = true;
			foreach(int s in Tron._.pointsByPlayerId.Values) {
				if (score < s) {
					won = false;
				}
			}
			status.text = won ? "YOU WIN!" : "WOU LOOSE!";
		} else {
			status.text = "";
			status.gameObject.active = false;
		}
	}

	public void OnStatusClick() {
		Debug.Log("RESET");
		Tron._.RestartGame();
	}
}
