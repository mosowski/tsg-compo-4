﻿Shader "Tron/GradientMap (+)" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_GradTex ("Grad (RGB)", 2D) = "white" {}
		_YScale ("YScale", Float) = 1
	}
	SubShader {
		Tags { 
			"Queue"="Transparent"
			"RenderType"="Transparent" 
			"IgnoreProjector"="True"
		}
		Pass {
			Lighting Off
			ZWrite Off
			Fog { Mode Off }
			Blend One One 
			Cull Off 
			
			CGPROGRAM
#pragma exclude_renderers d3d11 xbox360
#pragma fragment frag 
#pragma vertex vert

#include "UnityCG.cginc"
sampler2D _MainTex;
float4 _MainTex_ST;
sampler2D _GradTex;
float4 _GradTex_ST;
float _YScale;

struct appdata {
	float4 vertex : POSITION;
	float2 texcoord : TEXCOORD0;
	float4 color : COLOR;
};

struct v2f {
	float2 uv : TEXCOORD0;
	float4 pos : SV_POSITION;
	float4 color : COLOR;
};

v2f vert(appdata v) {
	v2f o;
#if defined(PIXELSNAP_ON) && !defined(SHADER_API_FLASH)
	v.vertex = UnityPixelSnap(v.vertex);
#endif
	o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
	o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
	o.color = v.color;
	return o;
}

half4 frag(v2f i) : COLOR {
	half4 s = tex2D(_MainTex, i.uv);
	half4 c = tex2D(_GradTex, float2(s.r, s.g)) * _YScale;
	return c * i.color;
}
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
