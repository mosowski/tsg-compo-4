﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Tron {

	public class BallCom : MonoBehaviour {
		[HideInInspector] public Ball ball;

		GameObject ringoPart1;
		GameObject ringoPart2;

		GameObject glow;

		GameObject trail;

		bool init = false;


		void Awake() {
			ringoPart1 = transform.Find("Ringo").gameObject;
			ringoPart2 = transform.Find("RingoLight").gameObject;
			trail = transform.Find("Trail").gameObject;
			glow = transform.Find("Glow").gameObject;

			MagicTrail m = trail.GetComponent<MagicTrail>();
			m.material = (Material)Instantiate(m.material);

			MeshRenderer mr = ringoPart1.GetComponent<MeshRenderer>();
			mr.material = (Material)Instantiate(mr.material);

			mr = ringoPart2.GetComponent<MeshRenderer>();
			mr.material = (Material)Instantiate(mr.material);

			mr = glow.GetComponent<MeshRenderer>();
			mr.material = (Material)Instantiate(mr.material);

		}
		
		void Update() {
			float timeOffset = Time.time - ball.State.timestamp;
			timeOffset = Mathf.Min(1, timeOffset / 0.02f);
			transform.localPosition = new Vector3(ball.State.pos.x + ball.State.vel.x * timeOffset, 0.5f, ball.State.pos.y + ball.State.vel.y * timeOffset);
			if (!init) {
				trail.GetComponent<MagicTrail>().Clear();
				init = true;
			}
			//ringoPart1.transform.Rotate(0,10,0);
		}

		void Start() {
			OwnerChanged();
		}

		public void OwnerChanged() {
			trail.GetComponent<MagicTrail>().material.SetTexture("_GradTex", _.gradientByPlayerId[ball.State.owner]);
			ringoPart1.GetComponent<MeshRenderer>().material.SetTexture("_GradTex", _.gradientByPlayerId[ball.State.owner]);
			ringoPart2.GetComponent<MeshRenderer>().material.SetTexture("_GradTex", _.gradientByPlayerId[ball.State.owner]);
			glow.GetComponent<MeshRenderer>().material.SetTexture("_GradTex", _.gradientByPlayerId[ball.State.owner]);
		}
	}
}
