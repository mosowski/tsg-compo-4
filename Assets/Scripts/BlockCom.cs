﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Tron {

	public class BlockCom : MonoBehaviour {
		[HideInInspector] public Block block;
		public Vector2[] collider;

		void Awake() {
		}
		
		void Update() {
			transform.localPosition = new Vector3(block.pos.x, 0, block.pos.y);
		}

		public void StartDestructionAnim() {
			GameObject b = transform.Find("SquareBlock").gameObject;
			Vector3 p = b.transform.localPosition;
			Quaternion r = b.transform.localRotation;

			GameObject.Destroy(b);

			b= Instantiate(Resources.Load<GameObject>("Blocks/SquareBlockSplit")) as GameObject;
			b.transform.parent = transform;
			b.transform.localPosition = Vector3.zero;
			b.transform.localRotation = Quaternion.identity;
			GameObject.Destroy(gameObject, 0.68f); //XXX
		}
	}
}
