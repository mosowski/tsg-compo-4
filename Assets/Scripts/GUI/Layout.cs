﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Layout : MonoBehaviour {


	public int workHeight = 2045;
	public int workWidth = 1363;

	[HideInInspector]
	public double aspect;
	[HideInInspector]
	public int width;
	[HideInInspector]
	public int height;
	[HideInInspector]
	public Vector3 scale;

	protected GameObject container;
	private Dictionary<string, int> windowToLayer = new Dictionary<string, int>();
	private Dictionary<string, GameObject> windows = new Dictionary<string, GameObject>();
	private Stack<List<GameObject>> hiddenWindows = new Stack<List<GameObject>>();
	private List<string>[] layers;

	[HideInInspector]
	public Camera camera;

	private const int MAX_LAYERS = 21;
	private const int LAYER_DEPTH = 1000;
	private const int INDEX_DEPTH = 100;

	[HideInInspector]
	public GameObject anchor;
	
	private static Vector3 originalPos;

	virtual protected GameObject GetContainer() {
		return transform.Find("Container").gameObject;
	}

	public GameObject OpenWindow(string name, int layer = -1) {
		return OpenWindow(name, name, layer);
	}

	public GameObject OpenWindow(string name, string prefabName, int layer = -1) {
		
		if (windows.ContainsKey(name)) {
			return null;
		}
		Debug.Log("trying to open: " + name + " " + prefabName);
		GameObject window = NGUITools.AddChild(container, Resources.Load<GameObject>("GUI/Windows/" + prefabName+ "/" + prefabName));
		windows[name] = window;
		window.name = name;
		GUIWindow w = window.GetComponent<GUIWindow>();
		w.layout = this;
		if (layer == -1) {
			layer = w.layer;
		}
		windowToLayer[name] = layer;
		layers[layer].Add(name);
		int index = layers[layer].IndexOf(name);
		//UIPanel panel = window.AddComponent<UIPanel>();
		//panel.clipSoftness = new Vector2(0, 0);
		//panel.depth = 0;
		NGUITools.AdjustDepth(window, layer * LAYER_DEPTH + index * INDEX_DEPTH);
		Debug.Log("Opened window: " + name + ", layer: " + layer + ", index: " + index);
		return window;
	}

	public void CloseWindow(string name) {
		GameObject go = windows[name];
		if(go != null) {
			int layer = windowToLayer[name];
			windowToLayer.Remove(name);
			int index = layers[layer].IndexOf(name);
			for(int i = index;i<layers[layer].Count;i++) {
				NGUITools.AdjustDepth(windows[layers[layer][i]], -1 * INDEX_DEPTH);
			}
			layers[layer].Remove(name);
			NGUITools.Destroy(go);
			windows.Remove(name);
			Debug.Log("Closed window: " + name + ", layer: " + layer + ", index: " + index);
		}
	}

	public void CloseWindow(MonoBehaviour mb) {
		CloseWindow(mb.GetComponent<GUIWindow>().name);
	}

	public bool IsOnTop(GameObject go) {
		return IsOnTop(go.GetComponent<GUIWindow>().name);
	}

	public bool IsOnTop(string name) {
		int layer = windowToLayer[name];
		for (int i = layer + 1;i<MAX_LAYERS;i++) {
			if (layers[i].Count > 0) {
				return false;
			}
		}
		return layers[layer][layers[layer].Count-1] == name;
	}


	public void Show() {
		List<GameObject> unhiddenWindows = hiddenWindows.Pop();
		foreach (GameObject window in unhiddenWindows) {
			NGUITools.SetActive(window, true);
		}
	}

	public void Hide() {
		List<GameObject> currentWindows = new List<GameObject>();
		foreach (KeyValuePair<string,GameObject> item in windows) {
			if (item.Value.activeSelf) {
				currentWindows.Add(item.Value);
				NGUITools.SetActive(item.Value, false);
			}
		}
		//NGUITools.SetActive(gui3DLayer, false);
		//currentWindows.Add(gui3DLayer);
		hiddenWindows.Push(currentWindows);
	}

	virtual protected void Init() {
		camera = transform.Find("Camera").GetComponent<Camera>();
		layers = new List<string>[MAX_LAYERS];
		for (int i = 0;i<MAX_LAYERS;i++) {
			layers[i] = new List<string>();
		}
		container = GetContainer();
		Reposition();
		UICamera.onScreenResize += Reposition;
	}

	// Use this for initialization
	void Start () {
		Init();
		Tron._.layout = this;
		OpenWindow("HUD");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	virtual protected void Reposition() {
		height = workHeight;
		aspect = (double)Screen.height/(double)Screen.width;
		if(aspect >= 3f/2f) {
			height = (int)Math.Round(workWidth * aspect);
		}
		width = (int)Math.Round(height / (double)Screen.height * (double)Screen.width);
		GetComponent<UIRoot>().manualHeight = height;
		float s = (float)workHeight / height;
		scale = new Vector3(s,s,s);

	}

}
