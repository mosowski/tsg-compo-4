﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.Generic;

public class PHAtlasEditor : EditorWindow {

	[MenuItem("PHTools/Atlas Maker")]
	static void Init() {
		PHAtlasEditor window =  (PHAtlasEditor)EditorWindow.GetWindow(typeof(PHAtlasEditor), true, "PH Atlas Maker");
		Debug.Log("Opened PH Atlas Maker " + window);
	}

	public static void BatchBuildAll() {
		Debug.Log("Building atlases.");
		BuildAll(Directory.GetDirectories(Application.dataPath + "/" + PHSpriteAtlas.atlasSourceDir));
	}

	static void BuildAll(string[] atlasesFolders) {
		if (Directory.Exists(Application.dataPath + "/Resources/" + PHSpriteAtlas.atlasDir)) {
			Directory.Delete(Application.dataPath + "/Resources/" + PHSpriteAtlas.atlasDir, true);
		}
		Directory.CreateDirectory(Application.dataPath + "/Resources/" + PHSpriteAtlas.atlasDir);
		AssetDatabase.Refresh();

		Dictionary<string, AtlasGeneratorNode> nodes = new Dictionary<string, AtlasGeneratorNode>();
		for (int i = 0; i < atlasesFolders.Length; ++i) {
			ComputeAtlas(atlasesFolders[i], nodes);
		}

		XmlDocument xml = new XmlDocument();
		xml.AppendChild(xml.CreateElement("nodes"));
		AddElementsToAtlasXml(xml, nodes);
		WriteXml(xml);
		
		AssetDatabase.Refresh();
	}

	void OnGUI() {
		string[] atlasesFolders = Directory.GetDirectories(Application.dataPath + "/" + PHSpriteAtlas.atlasSourceDir);

		GUILayout.BeginVertical();

		if (GUILayout.Button("Engage!")) {
			BuildAll(atlasesFolders);
			PHAtlasStorage.Reset();
		}
		GUILayout.BeginHorizontal();
		GUILayout.BeginVertical();
		for (int i = 0; i < atlasesFolders.Length; ++i) {
			string atlasName = Path.GetFileName(atlasesFolders[i]);
			if (GUILayout.Button("Build " + atlasName)) {
				string atlasFile = Application.dataPath + "/Resources/" + PHSpriteAtlas.atlasDir + "/" + atlasName + ".png";
				if (Directory.Exists(atlasFile)) {
					Directory.Delete(atlasFile);
				}
				AssetDatabase.Refresh();

				XmlDocument xml = ReadXml();
				RemoveElementsFromAtlasXml(xml, atlasName);
				Dictionary<string, AtlasGeneratorNode> nodes = new Dictionary<string, AtlasGeneratorNode>();
				ComputeAtlas(atlasesFolders[i], nodes);
				AddElementsToAtlasXml(xml, nodes);
				WriteXml(xml);

				AssetDatabase.Refresh();
				PHAtlasStorage.Reset();
			}
		}
		GUILayout.EndVertical();
		GUILayout.BeginVertical();
		for (int i = 0; i < atlasesFolders.Length; ++i) {
			string atlasName = Path.GetFileName(atlasesFolders[i]);
			if (GUILayout.Button("Remove " + atlasName)) {
				string atlasFile = Application.dataPath + "/Resources/" + PHSpriteAtlas.atlasDir + "/" + atlasName + ".png";
				if (Directory.Exists(atlasFile)) {
					Directory.Delete(atlasFile);
				}
				XmlDocument xml = ReadXml();
				RemoveElementsFromAtlasXml(xml, atlasName);
				WriteXml(xml);

				AssetDatabase.Refresh();
			}
		}
		GUILayout.EndVertical();
		GUILayout.EndHorizontal();
		GUILayout.EndVertical();
	}

	static void RemoveElementsFromAtlasXml(XmlDocument xml, string atlasId) {
		XmlNodeList nodesToRemove = xml.SelectNodes("//node[@atlasId='" + atlasId + "']");
		foreach (XmlNode node in nodesToRemove) {
			node.ParentNode.RemoveChild(node);
		}
	}

	static void AddElementsToAtlasXml(XmlDocument xml, Dictionary<string, AtlasGeneratorNode> nodes) {
		XmlNode atlasXml = xml.SelectSingleNode("/nodes");
		foreach (KeyValuePair<string, AtlasGeneratorNode> item in nodes) {
			XmlElement itemXml = (XmlElement)atlasXml.AppendChild(xml.CreateElement("node"));
			AtlasGeneratorNode node = item.Value;
			itemXml.SetAttribute("id", item.Key);
			itemXml.SetAttribute("atlasId", node.atlasId);
			itemXml.SetAttribute("page", node.page.ToString());
			itemXml.SetAttribute("left", node.uvLeft.ToString());
			itemXml.SetAttribute("top", node.uvTop.ToString());
			itemXml.SetAttribute("right", node.uvRight.ToString());
			itemXml.SetAttribute("bottom", node.uvBottom.ToString());
			itemXml.SetAttribute("width", node.texture.width.ToString());
			itemXml.SetAttribute("height", node.texture.height.ToString());
		}
	}

	XmlDocument ReadXml() {
		byte[] bytes = File.ReadAllBytes(Application.dataPath + "/Resources/" + PHSpriteAtlas.atlasDir + "/nodes.xml");
		string xmlString = System.Text.UTF8Encoding.UTF8.GetString(bytes);
		XmlDocument xml = new XmlDocument();
		xml.LoadXml(xmlString);
		return xml;
	}

	static void WriteXml(XmlDocument xml) {
		byte[] xmlBytes = System.Text.UTF8Encoding.UTF8.GetBytes(xml.OuterXml);
		File.WriteAllBytes(Application.dataPath + "/Resources/" + PHSpriteAtlas.atlasDir + "/nodes.xml", xmlBytes);
	}

	static void ComputeAtlas(string path, Dictionary<string, AtlasGeneratorNode> totalNodes) {
		string atlasName = Path.GetFileName(path);
		string[] files = Directory.GetFiles(path, "*.png");
		int maxSize = 2048;
		Texture2D[] textures = new Texture2D[files.Length];
		for (int i = 0; i < files.Length; ++i) {
			Texture2D texture = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets" + files[i].Replace(Application.dataPath, "").Replace("\\", "/"), typeof(Texture2D));
			textures[i] = texture;
			if (texture.width > maxSize - 4 || texture.height > maxSize - 4) {
				Debug.LogError("[PHAtlas] Texture " + texture.name + " is too big.");
				return;
			}
		}

		Array.Sort(textures, (a, b) => b.width - a.width);

		List<AtlasGenerator> pages = new List<AtlasGenerator>();
		pages.Add(new AtlasGenerator(32, atlasName));
		int currentTexture = 0;
		while (currentTexture != textures.Length) {
			for (int i = 0; i < pages.Count; ++i) {
				AtlasGenerator atlas = pages[i];
				Texture2D texture = textures[currentTexture];
				if (atlas.Insert(texture, atlasName + "/" + texture.name, i) == false) {
					if (atlas.size < maxSize) {
						pages[i] = new AtlasGenerator(atlas.size * 2, atlasName);
						for (int j = 0; j < i; ++j) {
							pages[j] = new AtlasGenerator(maxSize, atlasName);
						}
						currentTexture = 0;
						break;
					} else if (i == pages.Count - 1) {
						pages.Add(new AtlasGenerator(32, atlasName));
						currentTexture = 0;
						break;
					}
				} else {
					currentTexture++;
					break;
				}
			}
		}

		for (int i = 0; i < pages.Count; ++i) {
			AtlasGenerator atlas = pages[i];
			string atlasFileName = PHSpriteAtlas.atlasDir + "/" + atlasName + i.ToString() + ".png";

			foreach (KeyValuePair<string, AtlasGeneratorNode> item in atlas.nodes) {
				if (totalNodes.ContainsKey(item.Key)) {
					Debug.LogWarning("[PHAtlas] Id " + item.Key + " is duplicated.");
				}
				totalNodes[item.Key] = item.Value;
			}

			Texture2D outTexture = atlas.Draw();
			byte[] textureBytes = outTexture.EncodeToPNG();
			DestroyImmediate(outTexture);
			File.WriteAllBytes(Application.dataPath + "/Resources/" + atlasFileName, textureBytes);
			Debug.Log("[PHAtlas] Saved " + atlasFileName);
			AssetDatabase.Refresh();
			AssetDatabase.ImportAsset("Assets/" + atlasFileName);

			TextureImporter textureImporter = (TextureImporter)AssetImporter.GetAtPath("Assets/Resources/" + atlasFileName);
			textureImporter.textureType = TextureImporterType.Advanced;
			textureImporter.maxTextureSize = 2048;
			textureImporter.mipmapEnabled = false;
			textureImporter.alphaIsTransparency = true;
			textureImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;
			AssetDatabase.ImportAsset("Assets/Resources/" + atlasFileName);
		}
	}

	public static string Inspector(string label, string id) {
		Texture2D oldTexture = null;	
		string texturePath = PHSpriteAtlas.GetAssetPathForNodeId(id);
		if (!string.IsNullOrEmpty(texturePath)) {
			oldTexture = (Texture2D)AssetDatabase.LoadAssetAtPath(texturePath, typeof(Texture2D));
		}
		Texture2D newTexture = (Texture2D)EditorGUILayout.ObjectField(label, oldTexture, typeof(Texture2D), false, null);
		if (newTexture != oldTexture) {
			return PHSpriteAtlas.GetNodeIdForAssetPath(AssetDatabase.GetAssetPath(newTexture)); 
		}
		return id;
	}
}

class AtlasGenerator {
	public string atlasId;
	public AtlasGeneratorNode root;
	public Dictionary<string, AtlasGeneratorNode> nodes;
	public int size;

	public AtlasGenerator(int size, string id) {
		this.atlasId = id;
		this.size = size;
		root = new AtlasGeneratorNode(0, 0, size, size);
		nodes = new Dictionary<string, AtlasGeneratorNode>();
	}

	public bool Insert(Texture2D texture, string key, int page) {
		AtlasGeneratorNode node = root.Insert(texture);
		if (node != null) {
			nodes[key] = node;
			node.ComputeUV(root.width, root.height);
			node.atlasId = atlasId;
			node.page = page;
			return true;
		} else {
			return false;
		}
	}

	public Texture2D Draw() {

		Color32[] destPixels = new Color32[root.width * root.height];

		foreach (KeyValuePair<string, AtlasGeneratorNode> item in nodes) {
			AtlasGeneratorNode node = item.Value;
			Texture2D texture = node.texture;

			string sourceTexturePath = AssetDatabase.GetAssetPath(texture);
			TextureImporter textureImporter = (TextureImporter)AssetImporter.GetAtPath(sourceTexturePath);
			textureImporter.isReadable = true;
			AssetDatabase.ImportAsset(sourceTexturePath);

			Color32[] sourcePixels = texture.GetPixels32();

			for (int i = 0; i < texture.width; ++i) {
				for (int j = 0; j < texture.height; ++j) {
					destPixels[root.width * (node.y + 1 + j) + (node.x + 1 + i)] = sourcePixels[texture.width * j + i];
				}
			}
			for (int i = 0; i < node.width; ++i) {
				destPixels[root.width * node.y + (node.x + i)] = destPixels[root.width * (node.y + 1) + (node.x + i)];
				destPixels[root.width * (node.y + texture.height + 1) + (node.x + i)] = destPixels[root.width * (node.y + texture.height) + (node.x + i)];
			}
			for (int i = 0; i < node.height; ++i) {
				destPixels[root.width * (node.y + i) + node.x] = destPixels[root.width * (node.y + i) + node.x + 1];
				destPixels[root.width * (node.y + i) + node.x + texture.width + 1] = destPixels[root.width * (node.y + i) + node.x + texture.width];
			}
		}

		Texture2D atlas = new Texture2D(root.width, root.height);
		atlas.SetPixels32(destPixels);
		atlas.Apply();

		return atlas;
	}
}

class AtlasGeneratorNode {
	public int x;
	public int y;
	public int width;
	public int height;
	public string atlasId;
	public int page;
	public Texture2D texture;
	public AtlasGeneratorNode left;
	public AtlasGeneratorNode right;

	public float uvLeft;
	public float uvTop;
	public float uvRight;
	public float uvBottom;

	public AtlasGeneratorNode(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		texture = null;
	}

	public void ComputeUV(float rootWidth, float rootHeight) {
		uvLeft = (float)(x+1) / rootWidth;
		uvTop = (float)(y+1) / rootHeight;
		uvRight = (float)(x + texture.width) / rootWidth;
		uvBottom = (float)(y + texture.height) / rootHeight;
	}

	public AtlasGeneratorNode Insert(Texture2D texture) {
		if (this.texture != null) {
			AtlasGeneratorNode node = left.Insert(texture);
			if (node == null) {
				node = right.Insert(texture);
			}
			return node;
		} else {
			int textureWidth = texture.width + 2;
			int textureHeight = texture.height + 2;
			if (textureWidth <= width && textureHeight <= height) {
				this.texture = texture;
				left = new AtlasGeneratorNode(x, y + textureHeight, textureWidth, height - textureHeight);
				right = new AtlasGeneratorNode(x + textureWidth, y, width - textureWidth, height);
				return this;
			}
			return null;
		}
	}
}

