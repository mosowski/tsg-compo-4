﻿using UnityEngine;
using UnityEditor;
using System.Collections;

class PhHandles2D {

	static int selectedId = -1;

	public static Vector2 PositionHandle(int id, Vector2 pos) {
		switch (Event.current.type) {
			case EventType.mouseDown:
				if (Vector2.Distance(Event.current.mousePosition, pos) < 8) {
					selectedId = id;
					Event.current.Use();
				}
				break;
			case EventType.mouseUp:
				if (selectedId == id) {
					selectedId = -1;
					Event.current.Use();
				}
				break;
			case EventType.mouseDrag:
				if (selectedId == id) {
					pos = Event.current.mousePosition;
					Event.current.Use();
				}
				break;
			case EventType.repaint:
				EditorGUI.DrawRect(new Rect(pos.x - 5, pos.y -5, 10, 10), selectedId == id ? Color.red : Color.blue);
				break;
		}
		return pos;
	}

	static Texture2D lineTex;
 
    public static void DrawLine(Rect rect) { DrawLine(rect, GUI.contentColor, 1.0f); }
    public static void DrawLine(Rect rect, Color color) { DrawLine(rect, color, 1.0f); }
    public static void DrawLine(Rect rect, float width) { DrawLine(rect, GUI.contentColor, width); }
    public static void DrawLine(Rect rect, Color color, float width) { DrawLine(new Vector2(rect.x, rect.y), new Vector2(rect.x + rect.width, rect.y + rect.height), color, width); }
    public static void DrawLine(Vector2 pointA, Vector2 pointB) { DrawLine(pointA, pointB, GUI.contentColor, 1.0f); }
    public static void DrawLine(Vector2 pointA, Vector2 pointB, Color color) { DrawLine(pointA, pointB, color, 1.0f); }
    public static void DrawLine(Vector2 pointA, Vector2 pointB, float width) { DrawLine(pointA, pointB, GUI.contentColor, width); }
    public static void DrawLine(Vector2 pointA, Vector2 pointB, Color color, float width) {
		if (Vector2.Distance(pointA, pointB) > 0) {
			Matrix4x4 matrix = GUI.matrix;
			if (!lineTex) { lineTex = new Texture2D(1, 1); }
	 
			Color savedColor = GUI.color;
			GUI.color = color;
			float angle = Vector3.Angle(pointB - pointA, Vector2.right);
			if (pointA.y > pointB.y) { angle = -angle; }
			GUIUtility.ScaleAroundPivot(new Vector2((pointB - pointA).magnitude, width), new Vector2(pointA.x, pointA.y + 0.5f));
			GUIUtility.RotateAroundPivot(angle, pointA);
			GUI.DrawTexture(new Rect(pointA.x, pointA.y, 1, 1), lineTex);
			GUI.matrix = matrix;
			GUI.color = savedColor;
		}
    }

}
