﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CanEditMultipleObjects]
[CustomEditor(typeof(UIPHSprite))]
public class UIPHSpriteEditor : UIWidgetInspector {

	static public UIPHSprite phSprite;

	protected override void OnEnable () { 
		base.OnEnable();
		phSprite = mWidget as UIPHSprite; 
	}

	public override void OnInspectorGUI() {
		base.OnInspectorGUI();

		phSprite.AtlasNodeId = PHAtlasEditor.Inspector("Atlas sprite: ", phSprite.AtlasNodeId);

		phSprite.BaseMaterial = (Material)EditorGUILayout.ObjectField("Base Material: ", phSprite.BaseMaterial, typeof(Material), false);
	}
	
	protected override void DrawCustomProperties ()
	{
		GUILayout.Space(6f);

		SerializedProperty sp = NGUIEditorTools.DrawProperty("Sprite Type", serializedObject, "mType", GUILayout.MinWidth(20f));

		EditorGUI.BeginDisabledGroup(sp.hasMultipleDifferentValues);
		{
			UIPHSprite.Type type = (UIPHSprite.Type)sp.intValue;

			if (type == UIPHSprite.Type.Simple)
			{
				NGUIEditorTools.DrawProperty("Flip", serializedObject, "mFlip");
			}
			else if (type == UIPHSprite.Type.Sliced)
			{
				NGUIEditorTools.DrawProperty("Flip", serializedObject, "mFlip");
				sp = serializedObject.FindProperty("centerType");
				bool val = (sp.intValue != (int)UIPHSprite.AdvancedType.Invisible);

				if (val != EditorGUILayout.Toggle("Fill Center", val))
				{
					sp.intValue = val ? (int)UIPHSprite.AdvancedType.Invisible : (int)UIPHSprite.AdvancedType.Sliced;
				}
			}
			else if (type == UIPHSprite.Type.Filled)
			{
				NGUIEditorTools.DrawProperty("Flip", serializedObject, "mFlip");
				NGUIEditorTools.DrawProperty("Fill Dir", serializedObject, "mFillDirection", GUILayout.MinWidth(20f));
				GUILayout.BeginHorizontal();
				GUILayout.Space(4f);
				NGUIEditorTools.DrawProperty("Fill Amount", serializedObject, "mFillAmount", GUILayout.MinWidth(20f));
				GUILayout.Space(4f);
				GUILayout.EndHorizontal();
				NGUIEditorTools.DrawProperty("Invert Fill", serializedObject, "mInvert", GUILayout.MinWidth(20f));
			}
			else if (type == UIPHSprite.Type.Advanced)
			{
				NGUIEditorTools.DrawProperty("  - Left", serializedObject, "leftType");
				NGUIEditorTools.DrawProperty("  - Right", serializedObject, "rightType");
				NGUIEditorTools.DrawProperty("  - Top", serializedObject, "topType");
				NGUIEditorTools.DrawProperty("  - Bottom", serializedObject, "bottomType");
				NGUIEditorTools.DrawProperty("  - Center", serializedObject, "centerType");
				NGUIEditorTools.DrawProperty("Flip", serializedObject, "mFlip");
			}

			if (type == UIPHSprite.Type.Sliced || type == UIPHSprite.Type.Advanced) {
				NGUIEditorTools.DrawProperty("Border Left", serializedObject, "borderLeft");
				NGUIEditorTools.DrawProperty("Border Right", serializedObject, "borderRight");
				NGUIEditorTools.DrawProperty("Border Top", serializedObject, "borderTop");
				NGUIEditorTools.DrawProperty("Border Bottom", serializedObject, "borderBottom");
				NGUIEditorTools.DrawProperty("Border Scale", serializedObject, "borderScale");
			}
		}
		EditorGUI.EndDisabledGroup();

		GUILayout.Space(4f);
		base.DrawCustomProperties();
	}

	[MenuItem("PHTools/Create/PH Sprite &#p", false, 6)]
	static public void AddSprite()
	{
		GameObject go = NGUIEditorTools.SelectedRoot(true);

		if (go != null)
		{
			Selection.activeGameObject = UIPHSpriteEditor.AddPHSprite(go).gameObject;
		}
		else
		{
			Debug.Log("You must select a game object first.");
		}
	}

	static public UIPHSprite AddPHSprite(GameObject go)
	{
		UIPHSprite w = NGUITools.AddWidget<UIPHSprite>(go);
		w.name = "PHSprite";
		w.width = 100;
		w.height = 100;
		w.MakePixelPerfect();
		return w;
	}
}
