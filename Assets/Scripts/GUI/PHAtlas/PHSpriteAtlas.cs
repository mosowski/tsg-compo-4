﻿using UnityEngine;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
[InitializeOnLoad]
public class PHAtlasStorage {
	public static Dictionary<string, Texture2D> textures;
	public static Dictionary<string, PHSpriteAtlasNode> globalNodes;
	public static Dictionary<string, Material> materials;
	public static bool isUpToDate;

	static PHAtlasStorage() {
		Reset();
		EditorApplication.playmodeStateChanged += OnPlayModeChanged;
	}

	static void OnPlayModeChanged() {
		if (!EditorApplication.isPlaying) {
			Debug.Log("[PHAtlasStorage] Stopped playing");
			Reset();
		}
	}

	public static void Reset() {
		textures= new Dictionary<string, Texture2D>();
		globalNodes = new Dictionary<string, PHSpriteAtlasNode>();
		materials = new Dictionary<string, Material>();
		isUpToDate = false;
		Debug.Log("[PHAtlasStorage] Reset");
		PHAtlasStorage.NotifySprites();
	}

	public static void NotifySprites() {
		foreach (UIPHSprite sprite in Object.FindObjectsOfType<UIPHSprite>()) {
			sprite.OnAtlasChanged();
		}
	}
}
#else 
public class PHAtlasStorage {
	public static Dictionary<string, Texture2D> textures= new Dictionary<string, Texture2D>();
	public static Dictionary<string, PHSpriteAtlasNode> globalNodes = new Dictionary<string, PHSpriteAtlasNode>();
	public static Dictionary<string, Material> materials = new Dictionary<string, Material>();
	public static bool isUpToDate = false;
}
#endif

[ExecuteInEditMode]
public class PHSpriteAtlas : MonoBehaviour {

	public static string atlasSourceDir = "AtlasSource";
	public static string atlasDir = "Atlases";

	static void LoadAtlasesInfo() {
		if (!PHAtlasStorage.isUpToDate) {
			TextAsset textAsset = Resources.Load<TextAsset>(atlasDir + "/nodes");
			XmlDocument xml = new XmlDocument();
			xml.LoadXml(textAsset.text);
			XmlNodeList nodesXml = xml.DocumentElement.SelectNodes("/nodes/node");
			for (int i = 0; i < nodesXml.Count; ++i) {
				PHAtlasStorage.globalNodes[nodesXml[i].Attributes["id"].Value] = new PHSpriteAtlasNode(nodesXml[i]);
				Debug.Log("[PHSpriteAtlas] Loaded node " + nodesXml[i].Attributes["id"].Value);
			}
			PHAtlasStorage.isUpToDate = true;
			Debug.Log("[PHSpriteAtlas] Loaded info.");
		}
	}

	public static string GetAssetPathForNodeId(string nodeId) {
		return "Assets/" + atlasSourceDir + "/" + nodeId +  ".png";
	}

	public static string GetNodeIdForAssetPath(string assetPath) {
		if (assetPath.IndexOf("/") >= 0) {
			string fileName = assetPath.Substring(assetPath.IndexOf(atlasSourceDir) + atlasSourceDir.Length + 1);
			return fileName.Substring(0, fileName.IndexOf(".png"));
		} else {
			return assetPath;
		}
	}

	public static Material GetMaterial(PHSpriteAtlasNode node, Material baseMaterial) {
		string materialId = node.atlasId + node.page +  baseMaterial.name;
		if (PHAtlasStorage.materials.ContainsKey(materialId)
			&& PHAtlasStorage.materials[materialId] != null) {
			return PHAtlasStorage.materials[materialId];
		} else {
			Material material = Instantiate(baseMaterial) as Material;
			Texture2D texture = PHAtlasStorage.textures[node.atlasId + node.page];
			material.mainTexture = texture;
			PHAtlasStorage.materials[materialId] = material;
			Debug.Log("[PHSpriteAtlas] Created material " + materialId);
			return material;
		}
	}

	static Texture2D LoadTexture(string textureName) {
		Debug.Log("[PHSpriteAtlas] Loading texture " + textureName);
		Texture2D texture = Resources.Load<Texture2D>(atlasDir + "/" + textureName);
		PHAtlasStorage.textures[textureName] = texture;
		return texture;
	}
	
	public static PHSpriteAtlasNode GetNode(string nodeId) {
		LoadAtlasesInfo();
		if (PHAtlasStorage.globalNodes.ContainsKey(nodeId)) {
			PHSpriteAtlasNode node = PHAtlasStorage.globalNodes[nodeId];

			if (node.texture != null) {
				return node;
			} else {
				string textureName = node.atlasId + node.page;
				if (!PHAtlasStorage.textures.ContainsKey(textureName)) {
					LoadTexture(textureName);
				}
				node.texture = PHAtlasStorage.textures[textureName];
				return node;
			}
		} else {
			Debug.LogError("[PHSpriteAtlas] Missing node: " + nodeId);
			return null;
		}
	}
}


public class PHSpriteAtlasNode {
	public string id;
	public string atlasId;
	public int page;
	public float left;
	public float top;
	public float right;
	public float bottom;
	public float width;
	public float height;
	public Texture2D texture;

	public Rect rect;

	public PHSpriteAtlasNode(XmlNode xml) {
		texture = null;
		id = xml.Attributes["id"].Value;
		atlasId = xml.Attributes["atlasId"].Value;
		page = int.Parse(xml.Attributes["page"].Value);
		left = float.Parse(xml.Attributes["left"].Value);
		top = float.Parse(xml.Attributes["top"].Value);
		right = float.Parse(xml.Attributes["right"].Value);
		bottom = float.Parse(xml.Attributes["bottom"].Value);
		width = float.Parse(xml.Attributes["width"].Value);
		height = float.Parse(xml.Attributes["height"].Value);

		rect = new Rect(left, top, right-left, bottom-top);
	}
}

#if UNITY_EDITOR
public class PHAtlasTexturePreprocessor : AssetPostprocessor {
	public void OnPreprocessTexture() {
		if (assetPath.IndexOf(PHSpriteAtlas.atlasSourceDir) != -1) {
			TextureImporter ti = (TextureImporter)assetImporter;
			ti.textureType = TextureImporterType.Advanced;
			ti.mipmapEnabled = false;
			ti.textureFormat = TextureImporterFormat.RGBA32;
			ti.npotScale = TextureImporterNPOTScale.None;
		}
	}

}
#endif
