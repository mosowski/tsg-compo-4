﻿using UnityEngine;
using System.Collections;

public class ScreenBorderAnchor : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		UIWidget widget = GetComponent<UIWidget>();
		GUIWindow w =  transform.parent.GetComponent<GUIWindow>();
		if (w != null) {
			widget.height =  w.layout.height;
			widget.width =  w.layout.width;
		}
	}
}
