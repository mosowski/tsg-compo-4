﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Tron {

	public class Level {
		float width;
		float height;

		public GameObject view;

		public Collider leftWall;
		public Collider rightWall;
		public Collider topWall;
		public Collider bottomWall;

		public Level(string prefabId) {
			view = Object.Instantiate(Resources.Load<GameObject>(prefabId)) as GameObject;
			view.transform.parent = _.view.transform;

			_.camera = view.transform.Find("Camera").gameObject;

			foreach (BlockCom blockCom in view.GetComponentsInChildren<BlockCom>()) {
				_.AddBlock(blockCom.gameObject);
			}
		}

		public void SetSize(float w, float h, float e) {
			width = w;
			height = h;
			leftWall = new Collider();
			leftWall.SetFromArray(new Vector2[] {
				new Vector2(0, -e),
				new Vector2(0, height + e),
				new Vector2(-width, height + e),
				new Vector2(-width, -e)
			});
			rightWall = new Collider();
			rightWall.SetFromArray(new Vector2[] {
				new Vector2(2 * width, -e),
				new Vector2(2 * width, height + e),
				new Vector2(width, height + e),
				new Vector2(width, -e)
			});
			topWall = new Collider();
			topWall.SetFromArray(new Vector2[] {
				new Vector2(2 * width, height + e),
				new Vector2(2 * width, 2 * height),
				new Vector2(-width, 2 * height),
				new Vector2(-width, height + e)
			});
			bottomWall = new Collider();
			bottomWall.SetFromArray(new Vector2[] {
				new Vector2(2 * width, -height),
				new Vector2(2 * width, -e),
				new Vector2(-width, -e),
				new Vector2(-width, -height)
			});
		}
		
		public float Width { get { return width; } }
		public float Height { get { return height; } }
	}

	public class Block {
		public bool isDestroyed;
		public GameObject view;
		public Vector2 pos;
		public Collider collider;

		public Block(string prefabId) :
			this(Object.Instantiate(Resources.Load<GameObject>(prefabId)) as GameObject) {
		}

		public Block(GameObject view) {
			this.view = view;
			view.GetComponent<BlockCom>().block = this;
			view.transform.parent = _.view.transform;
			collider = new Collider();
			collider.SetFromArray(view.GetComponent<BlockCom>().collider);
		}

		public void Hit(BallState b) {
			_.pointsByPlayerId[b.owner] += _.pointsForBlock;
			view.GetComponent<BlockCom>().StartDestructionAnim();
			b.ball.PlayBlock();
			_.RemoveBlock(this);
		}

		public void Tick() {
			collider.Pos = pos;
			collider.Rotation = view.transform.eulerAngles.y * Mathf.Deg2Rad;
			collider.Scale = new Vector2(view.transform.localScale.x, view.transform.localScale.z);
			collider.ApplyTransform();
		}
	}

	public class Collider {
		Vector2[] shape;
		Vector2 pos;
		float rotation = 0;
		Vector2 scale = new Vector2(1,1);
		bool isDirty = false;

		public Vector2[] points;

		public Collider() {
		}

		public void SetFromArray(Vector2[] shape) {
			this.shape = shape;
			this.points = new Vector2[shape.Length];
			for (int i = 0; i < shape.Length; ++i) {
				points[i] = shape[i];
			}
		}

		public void ApplyTransform() {
			if (isDirty) {
				for (int i = 0; i < points.Length; ++i) {
					points[i] = new Vector2(
						(shape[i].x * scale.x * Mathf.Cos(rotation) - shape[i].y * scale.y * Mathf.Sin(rotation)) + pos.x,
						(shape[i].y * scale.y * Mathf.Cos(rotation) + shape[i].x * scale.x * Mathf.Sin(rotation)) + pos.y
					);
				}
				isDirty = false;
			}
		}

		public bool IsPointInside(Vector2 p) {
			ApplyTransform();

			bool isIn = false;
			int k = points.Length - 1;
			for (int i = 0; i < points.Length; k = i++) {
				if ((p.y > points[k].y && p.y <= points[i].y) || (p.y > points[i].y && p.y <= points[k].y)) {
					float tx = points[i].x + (points[i].x - points[k].x) * (p.y - points[k].y) / (points[i].y - points[k].y);
					if (tx >= p.x) {
						isIn = !isIn;
					}
				}
			}
			return isIn;
		}

		public Vector2 FindEdgesIntersection(Vector2 a, Vector2 b, Vector2 c, Vector2 d) {
			float dACy = a.y - c.y;
			float dDCx = d.x - c.x;
			float dACx = a.x - c.x;
			float dDCy = d.y - c.y;
			float dBAx = b.x - a.x;
			float dBAy = b.y - a.y;

			float p = dACy * dDCx - dACx * dDCy;
			float q = dBAx * dDCy - dBAy * dDCx;

			if (q == 0) {
				if (p == 0) {
					if (a.x >= c.x && a.x <= d.x) {
						return a;
					} else if (c.x >= a.x && c.x <= b.x) {
						return c;
					} else {
						return Vector2.zero;
					}
				} else {
					return Vector2.zero;
				}
			}

			float r = p / q;
			if (r < 0 || r > 1) {
				return Vector2.zero;
			}

			float s = (dACy * dBAx - dACx * dBAy) / q;
			if (s < 0 || s > 1) {
				return Vector2.zero;
			}

			return new Vector2(a.x + r * dBAx, a.y + r * dBAy);
		}

		private Vector2 FindCircleIntersection(Vector2 a, Vector2 b, Vector2 c, float radius) {
			float dx = b.x - a.x;
			float dy = b.y - a.y;

			float A = dx * dx + dy * dy;
			float B = 2 * (dx * (a.x - c.x) + dy * (a.y - c.y));
			float C = (a.x - c.x) * (a.x - c.x) + (a.y - c.y) * (a.y - c.y) - radius * radius;

			float delta = B * B - 4 * A * C;
			if (A <= 0.00001f || delta < 0) {
				return Vector2.zero; // Nope.
			} else if (delta == 0) {
				float t = -B / (2 * A);
				if (t >= 0 && t <= 1) {
					return new Vector2(a.x + t * dx, a.y + t * dy);
				}
			} else {
				float t = Mathf.Min((-B + Mathf.Sqrt(delta)) / (2 * A), (-B - Mathf.Sqrt(delta)) / (2 * A));
				if (t >= 0 && t <= 1) {
					return new Vector2(a.x + t * dx, a.y + t * dy);
				}
			}
			return Vector2.zero;
		}

		public Collision FindCollision(Vector2 p0, Vector2 p1, float r) {
			ApplyTransform();
			// edges
			int k = points.Length - 1;
			for (int i = 0; i < points.Length; k = i++) {
				Vector2 n = new Vector2(points[i].y - points[k].y, points[k].x - points[i].x).normalized;
				Vector2 x = FindEdgesIntersection(points[k] + r * n, points[i] + r * n, p0, p1);
				if (x != Vector2.zero) {
					return new Collision(x, n, p0);
				}
			}
			// corners
			k = points.Length - 2;
			int j = points.Length - 1;
			for (int i = 0; i < points.Length; k = j, j = i++) {
				Vector2 x = FindCircleIntersection(p0, p1, points[i], r);
				if (x != Vector2.zero) {
					Vector2 n = (x - points[i]).normalized;
					Vector2 nk = new Vector2(points[i].y - points[k].y, points[k].x - points[i].x).normalized;
					float crossk = nk.x * n.y - nk.y * n.x;
					Vector2 ni = new Vector2(points[j].y - points[i].y, points[i].x - points[k].x).normalized;
					float crossi = ni.x * n.y - ni.y * n.x;
					if (crossk > 0 && crossi < 0) {
						return new Collision(x, n, p0);
					}
				}
			}
			return null;
		}

		public Vector2[] Points { get { ApplyTransform(); return points; } }

		public Vector2 Pos { set { if (pos != value) { pos = value; isDirty = true; } } }
		public float Rotation { set { if (rotation != value) { rotation = value; isDirty = true; } } }
		public Vector2 Scale { set { if (scale != value) { scale = value; isDirty = true; } } }
	}

	public class Collision {
		public Vector2 p0;
		public Vector2 p;
		public Vector2 n;
		public float h;
		public object o;

		public Collision(Vector2 p, Vector2 n, Vector2 p0) {
			this.p = p;
			this.n = n;
			this.p0 = p0;
			h = (p - p0).magnitude;
		}
	}

	public class PaddleState {
		public Vector2 pos;
		public float vx;
		public float width;
		public string playerId;
		public float timestamp;

		public void Tick() {
			timestamp = Time.time;
			float fx = pos.x + vx;
			if (fx + width > _.level.Width) {
				fx -= fx + width - _.level.Width;
			}
			if (fx < 0) {
				fx = 0;
			}
			pos.x = fx;
			vx *= 0.1f;
		}
	}

	public class Paddle {
		public GameObject view;
		public PaddleState trueState;
		public Collider collider;

		public Paddle(string prefabId) {
			view = Object.Instantiate(Resources.Load<GameObject>(prefabId)) as GameObject;
			view.GetComponent<PaddleCom>().paddle = this;
			view.transform.parent = _.view.transform;
			collider = new Collider();
			collider.SetFromArray(view.GetComponent<PaddleCom>().collider);
		}

		public void Hit(BallState b) {
			b.SetOwnerId(State.playerId);
			b.ball.PlayPaddle();
		}

		public void Tick() {
			trueState.Tick();
			collider.Pos = trueState.pos;
			collider.Scale = new Vector2(Mathf.Abs(trueState.vx*50) + 1, 1);
		}

		public PaddleState State {
			get {
				return trueState;
			}
		}
	}

	public class BallState {
		public Vector2 pos;
		public Vector2 vel;
		public float timestamp;
		public float speed;

		public Ball ball;

		public float radius = 0.5f;
		public float hitFactor = 2.0f;
		public float spinFactor = 0.3f;
		public float spinFalloffFactor = 0.3f;

		public string owner;

		public void SetOwnerId(string id) {
			owner = id;
			ball.view.GetComponent<BallCom>().OwnerChanged();
		}

		void CalmDown() {
			float overSpeed = (1.0f + speed / vel.magnitude) * 0.5f;
			vel *= overSpeed;
		}

		Collision nearestCollision;

		void BeginCollisions() {
			nearestCollision = null;
		}

		void AddCollision(Collision c, object o) {
			if (c != null) {
				if (nearestCollision == null || nearestCollision.h > c.h) {
					nearestCollision = c;
					nearestCollision.o = o;
				}
			}
		}

		public void Tick() {
			timestamp = Time.time;
			if (Mathf.Abs(vel.x / vel.y) > 5.0f) {
				vel.y *= 1.21f;
			}

			CalmDown();

			Vector2 pos1 = pos + vel;

			BeginCollisions();
		   	AddCollision(_.level.leftWall.FindCollision(pos, pos1, radius), _.level);
			AddCollision(_.level.rightWall.FindCollision(pos, pos1, radius), _.level);

			AddCollision(_.level.bottomWall.FindCollision(pos, pos1, radius), _.level.bottomWall);
			AddCollision(_.level.topWall.FindCollision(pos, pos1, radius), _.level.topWall);

			for (int i = 0; i < _.blocks.Count; ++i) {
				if (!_.blocks[i].isDestroyed) {
					AddCollision(_.blocks[i].collider.FindCollision(pos, pos1,radius), _.blocks[i]);
				}
			}

			for (int i = 0; i < _.paddles.Count; ++i) {
				AddCollision(_.paddles[i].collider.FindCollision(pos, pos1, radius), _.paddles[i]);
			}

			if (nearestCollision != null) {
				ResolveCollision(ref pos1, nearestCollision);

				if (nearestCollision.o is Block) {
					Block b = (Block)nearestCollision.o;
					b.Hit(this);
					ball.PlayBlock();
				} else if (nearestCollision.o is Paddle) {
					Paddle p = (Paddle)nearestCollision.o;
					p.Hit(this);
					vel.x += p.State.vx * 0.1f;
					ball.PlayPaddle();
				} else if (nearestCollision.o == _.level.topWall) {
					_.pointsByPlayerId[_.players[1]] -= _.penaltyForBall;
					if (ball.State.owner == _.players[0]) {
						_.pointsByPlayerId[_.players[0]] += _.pointsForBall;
					}
					_.RemoveBall(ball);
				} else if (nearestCollision.o == _.level.bottomWall) {
					if (ball.State.owner != _.players[0]) {
						_.pointsByPlayerId[_.players[1]] += _.pointsForBall;
					}
					_.pointsByPlayerId[_.players[0]] -= _.penaltyForBall;
					_.RemoveBall(ball);
				} else if (nearestCollision.o == _.level) {
					ball.PlayWall();
				}
			}
			
			pos = pos1;
		}

		public void ResolveCollision(ref Vector2 p1, Collision c) {
			if (c == null) {
				return;
			}

			p1 = c.p + c.n * 0.01f;

			Vector2 projveln = Vector2.Dot(-vel, c.n) * c.n;
			vel += 2 * projveln;
		}
	}

	public class Ball { 
		public BallState trueState;

		public GameObject view;

		public void PlayWall() {
			view.GetComponents<AudioSource>()[0].Play();
		}

		public void PlayPaddle() {
			view.GetComponents<AudioSource>()[1].Play();
		}

		public void PlayBlock() {
			view.GetComponents<AudioSource>()[2].Play();
		}



		public Ball(string prefabId) {
			view = Object.Instantiate(Resources.Load<GameObject>(prefabId)) as GameObject;
			view.GetComponent<BallCom>().ball = this;
			view.transform.parent = _.view.transform;
		}

		public BallState State {
			get {
				return trueState;
			}
		}

		public void Tick() {
			trueState.Tick();
		}
	}
}
