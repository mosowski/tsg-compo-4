﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using LitJson;

namespace Tron {

	public class _ {
		public static GameObject view;
		public static GameObject camera;
		public static Level level;
		public static List<Ball> balls;
		public static List<Block> blocks;
		public static List<Paddle> paddles;
		public static Layout layout;
		public static Dictionary<string,Paddle> paddleByPlayerId;
		public static Dictionary<string, Texture> gradientByPlayerId ;

		public static Dictionary<string, int> pointsByPlayerId;

		public static List<string> players;

		public static List<string> gradients = new List<string>() {"Player1", "Player2"};

		public static List<Block> blocksToRemove;

		public static bool isPlaying;
		public static List<JsonData> actions;
		public static float lastTickTime;
		public static int localTickNum;
		public static int currentTickNum;
		public static int currentTurn;
		public static bool turnReported;
		public static bool turnReady;

		public static float swipeDirection = 1;

		public static string playerName = "";
		public static string roomName = "";

		public static float TickTime = 0.02f;
		public static int maxTicks = 50 * 300;

		public static int pointsForBlock = 5;
		public static int pointsForBall = 30;
		public static int penaltyForBall = 20;

		public static bool isSinglePlayer = true;
		public static float botMaxSpeed = 0.5f;

		public static int ballsRespawnsCount = 5;
		public static int respawnedBalls = 0;
		public static float ballSpeed = 0.15f;

		public static void Start(GameObject rootGameObject) {
			_.view = rootGameObject;
			balls = new List<Ball>();
			blocks = new List<Block>();
			paddles = new List<Paddle>();
			paddleByPlayerId = new Dictionary<string,Paddle>();
			gradientByPlayerId = new Dictionary<string, Texture>();
			pointsByPlayerId = new Dictionary<string, int>();

			blocksToRemove = new List<Block>();

			actions = new List<JsonData>();

			players = new List<string>();
			players.Add("Player1");
			players.Add("Player2");

			StartGame();
		}

		public static Ball AddBall(string prefabId, float x, float y, float vx, float vy, string owner) {
			Ball b = new Ball(prefabId);
		   	b.trueState = new BallState() {
				pos = new Vector2(x, y),
				vel = new Vector2(vx, vy),
				ball = b,
				speed = ballSpeed
			};
			b.trueState.SetOwnerId(owner);
			balls.Add(b);
			return b;
		}

		public static Ball RespawnBall(string prefabId, string playerId) {
			Paddle p = paddleByPlayerId[playerId];
			float vy = Random( ballSpeed*0.5f, ballSpeed*0.75f);
			float vx = Mathf.Sign(Random(-1f, 1f)) * (ballSpeed - vy);
			float y = 0;
			if (p.State.pos.y < 10) { //XXX:SOOO BAAAD
				y = 2;
			} else {
				y = 19;
			}
			respawnedBalls++;
			return AddBall(prefabId, p.State.pos.x + p.State.width/2 , y, vx, vy, playerId);
			
		}

		public static void RemoveBall(Ball b) {
			if (ballsRespawnsCount - respawnedBalls > 0) {
				RespawnBall("Balls/Ringo", b.State.owner);
			}
			GameObject.Destroy(b.view.gameObject);
			balls.Remove(b);
			b.view.active = false;
		}
		
		public static void AddBlock(string prefabId, float x, float y) {
			Block b = new Block(prefabId) {
				pos = new Vector2(x, y)
			};
			blocks.Add(b);
		}

		public static void AddBlock(GameObject view) {
			Block b = new Block(view) {
				pos = new Vector2(
					view.transform.localPosition.x,
					view.transform.localPosition.z
				)
			};
			blocks.Add(b);
		}

		public static void RemoveBlock(Block b) {
			b.isDestroyed = true;
			blocksToRemove.Add(b);
		}

		public static void AddPaddle(string prefabId, string playerId, float x, float y, float width) {
			Paddle p = new Paddle(prefabId);
			p.trueState = new PaddleState() {
				pos = new Vector2(x, y),
				width = width,
				playerId = playerId
			};
			paddleByPlayerId[playerId] = p;
			paddles.Add(p);
		}


		public static void SetLevel(string prefabId, float width, float height) {
			level = new Level(prefabId);
			level.SetSize(width, height, 2);
		}

		public static void SpawnBlocks(string spawnerId) {
			GameObject prefab = GameObject.Instantiate(Resources.Load<GameObject>(spawnerId)) as GameObject;
			foreach (BlockCom b in prefab.GetComponentsInChildren<BlockCom>()) {
				AddBlock(b.gameObject as GameObject);
			}
			GameObject.Destroy(prefab);
		}

		public static void Tick() {
			if (isPlaying) {
				TryGameTick();
			}
		}

		public static bool EndConditionMet() {
			if (currentTickNum > maxTicks) {
				return true;
			}

			if (balls.Count == 0) {
				return true;
			}

			return false;
		}


		static void TryGameTick() {
			if (EndConditionMet()) {
				return;
			}

			GameTick();
		}

		static Paddle GetBotPaddle() {
			return paddleByPlayerId[players[1]];
		}

		static int ticksToSpawn = 0;

		static void ShowIntro() {
			GameObject.Destroy(GameObject.Instantiate(Resources.Load<GameObject>("Levels/Intro")) as GameObject, 1.5f);
		}

		static void GameTick() {
			currentTickNum++;
			localTickNum = currentTickNum % 5;

			for (int i = 0; i < balls.Count; ++i) {
				balls[i].Tick();
			}
			for (int i = 0; i < blocks.Count; ++i) {
				blocks[i].Tick();
			}
			for (int i = 0; i < paddles.Count; ++i) {
				paddles[i].Tick();
			}

			for (int i = 0; i < blocksToRemove.Count; ++i) {
				blocks.Remove(blocksToRemove[i]);
			}
			blocksToRemove.Clear();

			if (ticksToSpawn > 0) {
				ticksToSpawn--;
				if (ticksToSpawn == 0) {
					SpawnBlocks("Levels/Spawners/Spawner" + Random(1, 5));
				}
			}
			if (blocks.Count == 0 && ticksToSpawn == 0) {
				ShowIntro();
				ticksToSpawn = 15;
			}

			if(isSinglePlayer) {
				Ball ball = null;
				foreach (Ball b in balls) {
					if (ball == null || b.State.pos.y > ball.State.pos.y) {
						ball = b;
					} 
				}
				if (ball == null) {
					return;
				}

				Paddle p = GetBotPaddle();
				float diff = ball.State.pos.x - (p.State.pos.x + p.State.width/2);
				p.State.pos.x += diff;
			}

		}

		public static void RestartGame() {
			for(int i = 0;i<players.Count;i++) {
				pointsByPlayerId[players[i]] = 0;
				paddleByPlayerId[players[i]].State.pos.x = 8.5f;
			}
			respawnedBalls = 0;
			currentTickNum = 0;
			localTickNum = 0;
			currentTurn = 0;
			blocksToRemove.Clear();
			foreach(Block block in blocks) {
				GameObject.Destroy(block.view.gameObject);
				RemoveBlock(block);
			}
			GameObject.Destroy(level.view.gameObject);
			SetLevel("Levels/Level", 20, 20);
			for (int i = 0; i < balls.Count; ++i) {
				RemoveBall(balls[i]);
			}
			RespawnBall("Balls/Ringo", players[0]);
			RespawnBall("Balls/Ringo", players[1]);
		}

		public static void StartGame() {
			_.SetLevel("Levels/Level", 20, 20);
			for(int i = 0;i<players.Count;i++) {
				pointsByPlayerId[players[i]] = 0;
				_.AddPaddle("Paddles/Paddle" + i, players[i], 8.5f, i*20+1, 3); //XXX
				gradientByPlayerId[players[i]] = Resources.Load<Texture>("Gradients/" + gradients[i]);
				if (players[i] == playerName) {
					if (i == 1) { // flip camera for player two
						Vector3 p = _.camera.transform.localPosition;
						p.z = _.level.Height - p.z;
						_.camera.transform.localPosition = p;
						Quaternion r = _.camera.transform.localRotation;
						r = Quaternion.AngleAxis(180, Vector3.up) * r;
						_.camera.transform.localRotation = r;
						_.swipeDirection = -1;
					}
				}
			}
			_.RespawnBall("Balls/Ringo", players[0]);
			_.RespawnBall("Balls/Ringo", players[1]);
			isPlaying = true;
			view.GetComponent<AudioSource>().Play();
		}

		public static void CommitSwipe(Paddle p, Vector2 delta) {
			if (!EndConditionMet()) {
				float dx = delta.x * swipeDirection * 0.3f;
				p.State.vx += dx;
			}
		}

		static uint seed = 1000000007;
		public static float Random(float a, float b) {
			seed *= 1000009;
			return ((float)(seed%10000)/10000)*(b-a) + a;
		}

		public static int Random(int a, int b) {
			seed *= 1000009;
			return (int)(seed%(1+b-a)) + a;
		}
	}
}
