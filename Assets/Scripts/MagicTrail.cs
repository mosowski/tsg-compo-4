﻿using UnityEngine;
using System.Collections;
 
public class MagicTrail : MonoBehaviour {
	class Node {
		public Vector3 position;
		public float time;
	}

	public Material material;
	public int numNodes;
	public float minDistance = 0;
	public AnimationCurve curve;
	public Gradient gradient;

	public new Camera camera;

	new Renderer renderer;
	MeshFilter meshFilter;
	Mesh mesh;
	Vector3[] vertices;
	int[] indices;
	Vector2[] uvs;
	Color[] colors;
	Node[] nodes;
	float timeLength;
	float lastAddTime;
	float addInterval;
	Vector3 lastPosition;
	bool isInitialized;

	void Awake() {
		renderer = gameObject.AddComponent<MeshRenderer>();
		renderer.material = material;

		meshFilter = gameObject.AddComponent<MeshFilter>();
		mesh = meshFilter.mesh;

		nodes = new Node[numNodes];
		for (int i = 0; i < numNodes; ++i) {
			nodes[i] = new Node();
		}

		vertices = new Vector3[numNodes * 2];
		uvs = new Vector2[numNodes * 2];
		colors = new Color[numNodes * 2];
		indices = new int[(numNodes - 1) * 6];

		timeLength = curve[curve.length - 1].time;
		addInterval = timeLength / numNodes;
		lastPosition = transform.position;

		InitMesh();
	}

	void InitMesh() {
		for (int i = 0; i < numNodes - 1; ++i) {
			indices[i * 6 + 0] = i * 2 + 0;
			indices[i * 6 + 1] = i * 2 + 1;
			indices[i * 6 + 2] = i * 2 + 2;

			indices[i * 6 + 3] = i * 2 + 3;
			indices[i * 6 + 4] = i * 2 + 2;
			indices[i * 6 + 5] = i * 2 + 1;
		}
	}

	void LateUpdate() {
		if (camera == null) {
			camera = Camera.mainCamera;
		}
		if (renderer.material != material) {
			renderer.material = material;
		}

		if (!isInitialized) {
			for (int i = 0; i < numNodes; ++i) {
				nodes[0].time = 0;
				nodes[0].position = Vector3.zero;
			}
			lastPosition = transform.position;
			isInitialized = true;
		}
		ShiftPoints();

		float now = Time.time;
		if (now - lastAddTime > addInterval && nodes[0].position.magnitude >= minDistance) {
			AddPoint();
			lastAddTime = now;
		}
		Vector3 cameraDirection = camera.transform.forward;
		Vector3 delta = Vector3.zero;
		for (int i = 0; i < numNodes; ++i) {
			float relativeTime = now - nodes[i].time;
			float width = 0;
			Color color = Color.black;
			if (relativeTime <= timeLength) {
				width = curve.Evaluate(relativeTime);
				color = gradient.Evaluate(relativeTime / timeLength);
			}
			if (i > 0) {
				Vector3 newDelta = (nodes[i].position - nodes[i-1].position).normalized;
				if (newDelta.sqrMagnitude > 0) {
					delta = newDelta;
				}
			}
			Vector3 perpendicular = Vector3.Cross(delta, nodes[i].position - camera.transform.position).normalized;
			vertices[i * 2 + 0] = nodes[i].position + perpendicular * width;
			vertices[i * 2 + 1] = nodes[i].position - perpendicular * width;
			uvs[i * 2 + 0] = new Vector2(relativeTime / timeLength, 0);
			uvs[i * 2 + 1] = new Vector2(relativeTime / timeLength, 1);
			colors[i * 2 + 0] = color;
			colors[i * 2 + 1] = color;
		}

		uvs[0] = uvs[1] = new Vector2(0, 0.5f);

		mesh.Clear();
		mesh.vertices = vertices;
		mesh.uv = uvs;
		mesh.colors = colors;
		mesh.triangles = indices;
		mesh.RecalculateBounds();
	}

	void ShiftPoints() {
		Vector3 delta = transform.position - lastPosition;
		lastPosition = transform.position;
		for (int i = 0; i < numNodes; ++i) {
			nodes[i].position -= delta;
		}
	}

	void AddPoint() {
		Node last = nodes[numNodes - 1];
		for (int i = numNodes - 1; i > 0 ; --i) {
			nodes[i] = nodes[i-1];
		}
		nodes[0] = last;
		nodes[0].position = Vector3.zero; //transform.position;
		nodes[0].time = Time.time;
	}

	public void Clear() {
		isInitialized = false;
		for (int i = 0; i < numNodes; ++i) {
			nodes[0].time = 0;
			nodes[0].position = Vector3.zero;
		}
	}
}
