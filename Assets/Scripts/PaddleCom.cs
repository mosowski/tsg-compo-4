﻿using UnityEngine;
using System.Collections;

namespace Tron {
	public class PaddleCom : MonoBehaviour {
		[HideInInspector] public Paddle paddle;
		public Vector2[] collider;

		GameObject glow;
		GameObject paddleg;

		void Awake() {
			glow = transform.Find("Glow").gameObject;
			paddleg = transform.Find("Paddle").gameObject;

			MeshRenderer mr = glow.GetComponent<MeshRenderer>();
			mr.material = (Material)Instantiate(mr.material);
			//mr.material.SetTexture("_GradTex", _.gradientByPlayerId[paddle.State.playerId]);

			mr = paddleg.GetComponent<MeshRenderer>();
			mr.material = (Material)Instantiate(mr.material);
			//mr.material.SetTexture("_GradTex", _.gradientByPlayerId[paddle.State.playerId]);
		}

		void Start() {
			glow.GetComponent<MeshRenderer>().material.SetTexture("_GradTex", _.gradientByPlayerId[paddle.State.playerId]);
			paddleg.GetComponent<MeshRenderer>().material.SetTexture("_GradTex", _.gradientByPlayerId[paddle.State.playerId]);
		}
		
		void Update() {
			float timeOffset = Time.time - paddle.State.timestamp;
			timeOffset = Mathf.Min(1, timeOffset / 0.02f);
			transform.localPosition = new Vector3(paddle.State.pos.x + paddle.State.vx * timeOffset, 0.0f, paddle.State.pos.y);
		}
	}
}
