﻿using UnityEngine;
using System.Collections;

namespace Tron {

	public class Starter : MonoBehaviour {

		void Start () {
			_.Start(gameObject);
		}
		
		void Update () {
			_.Tick();

			ProcessInput();
		}

#if UNITY_IPHONE
		Touch touch;
		bool isTouchDown;

		void ProcessInput() {
			if (!isTouchDown) {
				if (Input.touchCount > 0) {
					touch = Input.GetTouch(0);
					isTouchDown = true;
				}
			} else {
				touch = Input.GetTouch(0);
			   	if (touch.phase == TouchPhase.Ended) {
					isTouchDown = false;
				} else {
					float factor = 0.01f * 2048.0f/ (float)Screen.width;
					_.CommitSwipe(_.paddleByPlayerId["Player1"], touch.deltaPosition * factor);
				}
			}
		}
#else 
		bool isMouseDown;
		Vector3 lastMousePosition;

		void ProcessInput() {
			if (!isMouseDown) {
				if (Input.GetMouseButtonDown(0)) {
					isMouseDown = true;
					lastMousePosition = Input.mousePosition;
				}
			} else if (Input.GetMouseButtonUp(0)) {
				isMouseDown = false;
			} else {
				Vector3 delta = Input.mousePosition - lastMousePosition;
				_.CommitSwipe(_.paddleByPlayerId["Player1"], new Vector2(delta.x * 0.1f, delta.y));
				lastMousePosition = Input.mousePosition;
			}
		}

#endif
	}
}
