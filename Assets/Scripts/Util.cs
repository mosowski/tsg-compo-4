﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Maybe<T> {
}
public class Some<T> : Maybe<T> {
	public T t;
	public Some(T t) { this.t = t; }
}
public class None<T> : Maybe<T> {
}

public class Pair<Tx,Ty> {
	public Tx x;
	public Ty y;

	public Pair(Tx x, Ty y) {
		this.x = x;
		this.y = y;
	}
}

public static class Helpers {
	public static GameObject FindRecursive(this GameObject go, string name, bool safe = true) {
		foreach (Transform t in go.GetComponentsInChildren<Transform>()) {
			if (t.gameObject.name == name) {
				return t.gameObject;
			}
		}
		if (safe) {
			throw new Exception("Whoa! Child " + name + " not found in " + go.name);
		} else {
			return null;
		}
	}

	public static GameObject FindChild(this GameObject go, string name, bool safe = true) {
		Transform t = go.transform.Find(name);
		if (t != null) {
			return t.gameObject;
		} else if (safe) {
			throw new Exception("Whoa! Child " + name + " not found in " + go.name);
		} else {
			return null;
		}
	}
}

public class Util : MonoBehaviour {

	public static string[] colors = new string[] { "red", "green", "blue", "white", "black" };
	public static Dictionary<string,string> superEffectiveColors = new Dictionary<string,string>() {
		{ "red", "green" },
		{ "green", "blue" },
		{ "blue", "red" },
		{ "white", "black" },
		{ "black", "white" }
	};
	public static Dictionary<string, int> colorIndices = new Dictionary<string, int>() {
		{ "red", 0 },
		{ "green", 1 },
		{ "blue", 2 },
		{ "white", 3 },
		{ "black", 4 }
	};

	public static string UppercaseFirst(string s) {
		if (string.IsNullOrEmpty(s)) {
	    	return string.Empty;
		}
		return char.ToUpper(s[0]) + s.Substring(1);
    }

    public static Action After(int count, Action cb) {
    	int done = 0;
    	return () => {
    		done++;
    		if (done == count) {
    			cb();
    		}
    	};
    }

    public static void SetSprite(UISprite obj, string name) {
		if (name == null) {
			obj.atlas = null;
			obj.spriteName = null;
			return;
		}
		string[] tmp = name.Split('/');
		obj.atlas = GetAtlas(tmp[0]);
		if (obj.atlas != null) {
			obj.spriteName = tmp[1];
		}	
	}

	public static UIAtlas GetAtlas(string atlasName) {
		GameObject atlasGO = Resources.Load<GameObject>("Gui/Atlas/" + atlasName + "/" + atlasName);
		if (atlasGO == null) {
			//throw new Exception("no such atlas: " + atlasName);
			return null;
		}
		return atlasGO.GetComponent<UIAtlas>();
	}

	public static GameObject GetParentWindow(GameObject go) {
		if (go == null) {
			return null;
		}

		if (go.GetComponent<GUIWindow>() != null) {
			return go;
		}

		Transform parent = go.transform.parent;
		if (parent == null) {
			return null;
		}
		
		return GetParentWindow(parent.gameObject);
	}

	public static IEnumerable<T> FilterProperties<B,T>(IEnumerable<B> bs, System.Func<B,Maybe<T>> pred) {
		foreach (B b in bs) {
			Maybe<T> mt = pred(b);
			if (mt is Some<T>) {
				yield return ((Some<T>)mt).t;
			}
		}
	}

	public static T GetRandomUniform<T>(IEnumerable<T> ts) {
		int n = 1;
		T r = default(T);
		foreach (T t in ts) {
			int rand = UnityEngine.Random.Range(0,n);
			if (rand == 0) {
				r = t;
			}
			n++;
		}
		return r;
	}
}

