import js.Node;
import yawf.*;
import yawf.reflections.*;
import minject.Injector;
import data.*;
import logic.*;
import command.*;

class Arkanoid {

	private var connections:Map<String, Connection>;

	private var server:NodeNetServer;

	private var registeredCommands:Map<String, GenericCommand -> Connection -> Void>;

	public var players:Map<String, Player>;

	public var rooms:Map<String, Room>;


	public function start() {
		server.listen(4242);
	}

	public function new() {
		connections = new Map<String, Connection>();
		registeredCommands  = new Map<String, GenericCommand -> Connection -> Void>();
		players = new Map<String, Player>();
		rooms = new Map<String, Room>();

		server = Node.net.createServer(function (socket:NodeNetSocket) {
			var connection:Connection = new Connection(socket);

			var tmp:Connection = connections.get(connection.id);
			if (tmp != null) {
				Util.trace("duplicated connection id");
				terminate(tmp);
			}

			Util.trace("setting connection: " + connection.id);
			connections.set(connection.id, connection);


			var receivedData:String = "";
			var b = new Benchmark();
			socket.on('data', function(data:String) {
				Util.trace("came");
				b.check();
				var commandsToRun:Array<GenericCommand> = new Array<GenericCommand>();
				//Util.trace("receivedData: " + data);
				receivedData += data;

				var splitted:Array<String> = receivedData.split('#');
				for (i in 0...splitted.length-1) {
					//Util.trace("message: " + splitted[i]);
					var command:GenericCommand = decodeCommand(splitted[i]);
					commandsToRun.push(command);
					
				}
				b.check();

				receivedData = splitted.pop();
				for (command in commandsToRun) {
					if (registeredCommands.exists(command.name)) {
						registeredCommands[command.name](command, connection);
						b.check();
					} else {
						Util.trace("command: " + command.name + " does not exits");
					}
				}
			});

			socket.on('end', function() {
				Util.trace("end: " + connection.id);
			});

			socket.on('error', function(error:String) {
				Util.trace("error: " + connection.id);
				Util.trace(error);
			});

			socket.on('close', function() {
				Util.trace("close: " + connection.id);
				terminate(connection);
			});

		});
	}

	public function terminate(connection:Connection) {
		Util.trace("terminating " + connection.id);
		var player:Player = players[connection.id];
		if (player != null) {
			players.remove(connection.id);
			if (player.room != null) {
				var room:Room = player.room;
				if(room.owner == player) {
					for(p in room.players) {
						p.leaveRoom();
					}
				} else {
					player.leaveRoom();
				}
				if (room.players.length == 0) {
					rooms.remove(room.name);
				}
			}
		}
		connection.socket.destroy();
		connections.remove(connection.id);
	}

	private function decodeCommand(encoded:String):GenericCommand {
		var obj = Node.parse(encoded);
		return new GenericCommand(obj.name, obj.args);
	}

	private function createInjector(command:GenericCommand, connection:Connection):Injector {
		var injector:Injector = new Injector();
		injector.mapValue(GenericCommand, command);
		injector.mapValue(Connection, connection);
		injector.mapValue(Arkanoid, this);

		injector.mapValue(Players, injector.instantiate(Players));
		injector.mapValue(Rooms, injector.instantiate(Rooms));
		return injector;
	}

	public function register(commandsSet:Class<Dynamic>) {
		var classInfo:ClassInfo = Reflection.getClassInfo(commandsSet);

		var startFunc:ClassFieldInfo = classInfo.getFieldsByMeta("start").shift();

		var commandsFuncs:Array<ClassFieldInfo> = classInfo.getFieldsByMeta("command");


		Util.trace("registering commands:");
		for (commandFunc in commandsFuncs) {
			var key:String = commandFunc.getMeta("command")[0];
			Util.trace(key);


			var func:GenericCommand -> Connection -> Void;
			func = function (command:GenericCommand, connection:Connection) {
				Util.trace("called: " + key);
				var injector:Injector = createInjector(command, connection);
				var set:Dynamic = injector.instantiate(commandsSet);
				var d = Node.require('domain').create();

				var toRun:Void -> Void = function () {
					var startCb:Void -> Void = function () {
						Reflect.callMethod(set, Reflect.field(set, commandFunc.name), getArgs(commandFunc, command, null));
					}

					if (startFunc != null) {
						Reflect.callMethod(set, Reflect.field(set, startFunc.name), getArgs(startFunc, command, startCb));
					} else {
						startCb();
					}
				
				}
				d.on('error', function(err) {
					try {
						throw(err);
					} catch (e:String) {
						Util.trace(e);
						set.error(e);
					} catch (e:Dynamic) {
						var msg:String = "Unhandled error";
						Util.trace(msg);
						set.error(msg);
						throw (e);
					}
					
				});
				d.run(toRun);
			}
			registeredCommands.set(key, func);
		}
	}

	private function getArgs(func:ClassFieldInfo, command:GenericCommand, ret:Void -> Void):Array<Dynamic> {
		var res:Array<Dynamic> = new Array<Dynamic>();
		var args:List<FuncArg> = Reflection.getFuncArgs(func);
		if (args == null) {
			throw func.name + " should be a function";
		} else {
			for(arg in args) {
				switch(arg.type) {
					case Function(a, r):
						res.push(ret);
					default:
						res.push(ObjectMapper.fromPlainObjectUntyped(command.args.shift(), arg.type));
				}
			}
		}
		return res;
	}
}