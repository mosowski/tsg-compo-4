
package ;

import js.Node;

class Benchmark 
{
	public var previous:Dynamic;

	public function new () {
		previous = Node.process.hrtime();
	}

	public function check() {
		var next = Node.process.hrtime();
		var diff = [next[0] - previous[0], next[1] - previous[1]];
		trace('benchmark took %d nanoseconds', diff[0] * 1e9 + diff[1]);
		previous = next;
	}

}