
package ;

import yawf.*;
import command.*;

@:rtti
class CommandsSet 
{
	@inject
	public var connection:Connection;

	@inject
	public var command:GenericCommand;

	@inject
	public var app:Arkanoid;

	public function error(msg) {
		connection.sendCommand(new GenericCommand("error", [msg]));
	}

}