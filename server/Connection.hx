
package ;

import js.Node;
import yawf.*;
import command.*;

class Connection 
{

	public var id:String;
	public var socket:NodeNetSocket;

	public function new(socket:NodeNetSocket) {
		socket.setEncoding("ascii");
		id = socket.remoteAddress + ":" + socket.remotePort;
		this.socket = socket;
	}

	public function sendCommand(command:Command) {
		socket.write(ObjectMapper.toJson(command) + "#");
	}

}