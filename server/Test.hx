
package ;

import yawf.*;

@:rtti
class Test extends CommandsSet
{

	@start
	public function zzzz(cos:String, ret:Void->Void) {
		Util.trace("called before");
		Util.trace("cos is " + cos);
		ret();
	}

	@command("some")
	public function asd(ret:Void -> Void) {
		Util.trace("called asd");
		connection.socket.write("ugabuga", ret);
	}
	
}