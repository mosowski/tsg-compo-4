
package command;

@:rtti
class Command 
{
	@param
	public var name:String;

	public function new(name:String) {
		this.name = name;
	}
}