
package command;

import data.*;

@:rtti
class EnteredRoom extends Command
{
	@param
	public var room:Room;

	public function new (room:Room) {
		super("enteredRoom");
		this.room = room;
	}
	

}