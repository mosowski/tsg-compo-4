
package command;

import data.*;

@:rtti
class GameStarted extends Command
{
	@param
	public var players:Array<Player>;

	public function new (players:Array<Player>) {
		super("gameStarted");
		this.players = players;
	}
	

}