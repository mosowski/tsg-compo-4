
package command;

@:rtti
class GenericCommand extends Command
{
	@param
	public var args:Array<Dynamic>;

	public function new (name:String, args:Array<Dynamic>) {
		super(name);
		this.args = args;
	}
	

}