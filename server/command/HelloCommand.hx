
package command;

@:rtti
class HelloCommand extends Command 
{

	@param
	public var playerName:String;

	public function new (playerName:String) {
		super("hello");
		this.playerName = playerName;
	}

}