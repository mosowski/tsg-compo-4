
package command;

import data.*;

@:rtti
class PlayersList extends Command
{

	@param
	public var list:Array<Player>;

	public function new (list:Array<Player>) {
		super("playersList");
		this.list = list;
	}
	

}