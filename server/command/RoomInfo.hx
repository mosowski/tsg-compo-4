package command;

import data.*;

@:rtti
class RoomInfo extends Command
{

	@param
	public var room:Room;

	public function new (room:Room) {
		super("roomInfo");
		this.room = room;
	}
	

}