
package command;

import data.*;

@:rtti
class RoomsList extends Command
{

	@param
	public var list:Array<Room>;

	public function new (list:Array<Room>) {
		super("roomsList");
		this.list = list;
	}

}