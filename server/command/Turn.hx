
package command;

@:rtti
class Turn extends Command
{
	@param
	public var turn:Array<String>;
	
	public function new (turn:Array<String>) {
		super("turn");
		this.turn = turn;
	}

}