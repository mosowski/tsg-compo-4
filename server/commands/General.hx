
package commands;

import logic.*;
import command.*;

@:rtti
class General extends CommandsSet
{
	@inject
	public var players:Players;

	@inject
	public var rooms:Rooms;

	@command("hello")
	public function logIn(name:String) {
		players.logPlayer(name, connection);
		connection.sendCommand(new HelloCommand(name));
	}

	@command("playersList")
	public function playersList() {
		connection.sendCommand(new PlayersList(players.getPlayers()));	
	}

	@command("roomsList")
	public function roomsList() {
		connection.sendCommand(new RoomsList(rooms.getRooms()));
	}

}