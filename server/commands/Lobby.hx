
package commands;

import data.*;
import logic.*;
import command.*;
import yawf.*;

@:rtti
class Lobby extends CommandsSet
{

	@inject
	public var players:Players;

	@inject
	public var rooms:Rooms;

	private var player:Player;

	@start
	public function validatePlayer(ret:Void -> Void) {
		player = players.getPlayerByConnection(connection);
		if(player == null) {
			throw "player not logged in";
		} else {
			ret();
		}
	}	

	@command("enterRoom")
	public function enterRoom(roomName:String) {
		var room:Room = rooms.enterRoom(player, roomName);
		connection.sendCommand(new EnteredRoom(room));
		for(p in room.players) {
			p.connection.sendCommand(new RoomInfo(room));
		}
	}

	@command("roomInfo")
	public function roomInfo(roomName:String) {
		connection.sendCommand(new RoomInfo(rooms.getRoom(roomName)));
	}

	@command("startGame")
	public function startGame(roomName:String) {
		var room:Room = rooms.startGame(player, roomName);
		for(p in room.players) {
			p.connection.sendCommand(new GameStarted(room.players));
		}
	}

	@command("reportTurn")
	public function reportTurn(roomName:String, turn:Array<String>) {
		var room:Room = rooms.reportTurn(player, roomName, turn);
		if (room.ready()) {
			var tmp:Array<String> = new Array<String>();
			for(p in room.players) {
				tmp = tmp.concat(p.turn);	
				p.turn = null;
			}
			var turn = new Turn(tmp);
			for(p in room.players) {
				p.connection.sendCommand(turn);
			}
		}
	}

}