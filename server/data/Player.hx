
package data;

@:rtti
class Player 
{
	@param
	public var name:String;

	public var connection:Connection;

	public var turn:Array<String> = null;

	public var room:Room = null;

	public function new (name:String, connection:Connection) {
		this.name = name;
		this.connection = connection;
	}

	public function leaveRoom() {
		if (room != null) {
			room.players.remove(this);
			room = null;
		}
	}

}