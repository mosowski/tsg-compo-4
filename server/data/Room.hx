
package data;

@:rtti
class Room 
{
	@param
	public var name:String;

	@param
	public var players:Array<Player>;

	public var owner:Player;

	public var hasStarted:Bool = false;

	public function new(name:String, player:Player) {
		this.name = name;
		players = new Array<Player>();
		players.push(player);
		owner = player;
	}

	public function ready():Bool {
		for(player in players) {
			if(player.turn == null) {
				return false;
			}
		}
		return true;
	}

}