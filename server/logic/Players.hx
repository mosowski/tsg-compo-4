
package logic;

import minject.Injector;
import data.*;

class Players 
{
	@inject
	public var app:Arkanoid;
	
	public function getPlayerByConnection(connection:Connection):Player {
		return app.players.get(connection.id);
	}

	public function logPlayer(name:String, connection:Connection):Player {
		var player:Player = getPlayerByConnection(connection);
		if(player != null) {
			throw "player already hailed";
		} else {
			player = new Player(name, connection);
			app.players.set(connection.id, player);
			return player;
	    }
	}

	public function getPlayers():Array<Player> {
		return Lambda.array(app.players);
	}


}