
package logic;

import minject.Injector;
import data.*;
import yawf.*;

class Rooms 
{
	@inject
	public var app:Arkanoid;

	public function getRooms():Array<Room> {
		return Lambda.array(app.rooms);
	}

	public function enterRoom(player:Player, roomName:String):Room {
		var room:Room;
		if(app.rooms.exists(roomName)) {
			var room:Room = app.rooms[roomName];
			if (Lambda.indexOf(room.players, player) != -1) {
				throw "player already in room";
			} else if (room.players.length > 1) {
				throw "room full";
			} else {
				player.room = room;
				room.players.push(player);
				return room;
			}
		} else {
			room = new Room(roomName, player);
			app.rooms.set(roomName, room);
			return room;
		}
	}

	public function getRoom(name:String):Room {
		return app.rooms[name];
	}

	public function reportTurn(player:Player, roomName:String, turn:Array<String>):Room {
		var room:Room = getRoom(roomName);
		if (player.turn != null) {
			throw "player has already reported turn";
		}
		player.turn = turn;
		return room;
	}

	public function startGame(player:Player, roomName:String):Room {
		if(app.rooms.exists(roomName)) {
			var room:Room = app.rooms[roomName];
			if (room.hasStarted) {
				throw "room already started";
			} else if (room.owner == player) {
				room.hasStarted = true;
				return room;
			} else {
				throw "player is not an owner of room";
			}
		} else {
			throw "no such room";
		}
	}

}